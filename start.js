require('dotenv').config()

const express = require('express')
const path = require('path')
const app = express()

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'build')))

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/build/index.html'))
})

const port = process.env.PORT || 3121
app.listen(port)

console.log('[' + new Date().toLocaleString() + '] Frontend React Template started with PID ' + process.pid + ' on port ' + port)
// app.get('/', (req, res) => res.send('Hello World!'))

// Serve the static files from the React app
// app.use(express.static(path.join(__dirname, 'build')))
// app.listen(port, () => console.log(`Example app listening on port ${port}!`))
