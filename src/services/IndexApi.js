import GlobalAPI from './GlobalApi'
// import accountAPI from '../../account/services/api'

/* Kumpulan link API per page */
const globalAPI = GlobalAPI.create()

export {
  globalAPI
}
