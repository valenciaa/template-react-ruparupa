import { useQuery } from 'react-query'
import { globalAPI } from '../../IndexApi'
import queryWrapper from '../base'

export const getBannerSearchKey = 'getBannerSearch'
export function useGetBannerSearch (options) {
  return useQuery(getBannerSearchKey, queryWrapper(globalAPI.getBannerSearchBar), options)
}

export const getCategoryTree = 'getCategoryTree'
export function useGetCategoryTree (options) {
  return useQuery(getCategoryTree, queryWrapper(globalAPI.getCategory), options)
}
