import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { ConnectedRouter } from 'connected-react-router'
// import $ from 'jquery'

import { PersistGate } from 'redux-persist/integration/react'

import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import configureStore, { history } from './redux/index'

import { QueryClientProvider } from 'react-query'
import queryClient from './services/api/client'

const { store, persistor } = configureStore()

ReactDOM.render(
  <QueryClientProvider client={queryClient}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </PersistGate>
      </ConnectedRouter>
    </Provider>
  </QueryClientProvider>,
  document.getElementById('root')
)
