import rootSaga from '../sagas/index'
import CreateStore from './CreateStore'
import { connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'

// ALL REDUCER MUST BE IMPORTED HERE
import { reducer as userRedux } from './UserRedux'

export const history = createBrowserHistory()

export default initialState => {
  const reducers = {
    user: userRedux
  }

  const rootReducer = { ...reducers, router: connectRouter(history) }

  return CreateStore(rootReducer, rootSaga, initialState, history)
}
