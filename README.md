This Project is already compatible with using `React Hooks`, [Make sure to learn about it first, before continuing]
Don't forget to check on the <b>README</b> file for some notes and instructions.
You can change anything inside this project to adjust to your project requirements.
If there's something that needs to be removed or improved from this template, please contact the repository owner.
The feedback will help us to improve standardization in our projects in the future.
## Main Scripts Run

In the project directory, you can run:

### `npm run dev`
Runs the app in the development mode with scss that already compiled with watcher so they can detect every changes of css if you changes it.
If the css didn't change after hot reload, just repeat the `npm run dev` so the project might run from the start.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### NOTES
- If there any new endpoint you can put it inside Api.js
- The redux only storing the data that need to be used extensively
- You can use FetchingGroup.js to hit some endpoints that doesn't need to be store inside redux. Just import it and you ready to go. [Ex: import { fetchSomething } from '../../Services/FetchingGroup']
- Writting CSS suggested using BEM (Block Element Modifier)[http://getbem.com/introduction/]
- Only write your css inside the .sccs file

#### NEVER DO THIS
- Never changes the scss inside of plugins except plugins-dir.scss because it's not our custom scss.
- Never changes the codes from node-modules.
- Never write your new css inside index.css.
